// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.

//= require jquery/dist/jquery.min
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery.remotipart

//= require private_pub

//= require bootstrap-sprockets
//= require jasny-bootstrap/dist/js/jasny-bootstrap.min

//= require list.js/dist/list.min
//= require list.pagination.js/dist/list.pagination.min

//= require datatables.net/js/jquery.dataTables
//= require datatables.net-bs/js/dataTables.bootstrap

//= require sip.js/dist/sip.min

//= require main.js

