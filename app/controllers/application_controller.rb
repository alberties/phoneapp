class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_action :set_user

  respond_to :html, :json

  def authorize_admin
    redirect_to '/', alert: 'Acceso Denegado' unless current_user.admin?
  end

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  private

    def set_user
      gon.current_user = current_user
    end

  protected

      def configure_permitted_parameters
          devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :admin, :first_name, :last_name) }
          devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :current_password, :admin, :first_name, :last_name, :avatar, :status_id) }
      end
end
