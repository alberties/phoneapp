class CallsController < ApplicationController
  protect_from_forgery except: :create

  def create
    if !params[:external] || params[:external] != true
      if User.where(:sip_id => params[:number]).present?
        callee_id = User.find_by_sip_id(params[:number]).id
      elsif Outsider.where(:sip_id => params[:number]).present?
        callee_out_id = Outsider.find_by_sip_id(params[:number]).id
      else
        params[:external] = true
      end
    end

    @call = Call.create(
      call_type: params[:type], 
      caller_id: current_user.id, callee_id: callee_id, callee_out_id: callee_out_id,
      callee_sip: params[:number]+"@"+params[:server],
      external: params[:external])

    @start_call = true
    if @call.callee && @call.callee.status.receive_calls == false
      @start_call = false
      flash.now[:error] = "Usuario no disponible"
    end

    if @call.callee && @call.callee.sip_id == current_user.sip_id
      @start_call = false
      flash.now[:error] = "No puedes llamarte a ti mismo"
    end
  end

  def update
    @call = Call.find(params[:id])
    params[:duration] = @call.get_duration if params[:duration]
    @call.update(call_params)

    respond_to do |format|
      format.js   { render :layout => false }
    end
  end

  def show
    @call = Call.find(params[:id])
    @call.current_user_id = current_user.id
  end

  def get_call_id
    if current_user.status.receive_calls
      @caller = User.find_by_sip_id(params[:caller_sip_id])

      if !@caller
        @call = Call.create(
          :call_type => "audio",
          :callee_id => current_user.id, :callee_sip => current_user.sip_uri, 
          :external => true)
      else
        @call = Call.where(:caller_id => @caller.id).last
      end

      redirect_to controller: 'calls', action: 'show', id: @call.id, 
        format: 'js', 
        caller_sip_id: params[:caller_sip_id], caller_sip_name: params[:caller_sip_name]
    end
  end

  private
    def call_params
      params.permit(:duration, :answered)
    end
end

