class ChatsController < ApplicationController
	def index
	end

	def show
		@chat = Chat.find(params[:id])
		@lines = @chat.lines
	end

	def update
	end

	def create
		if Chat.between(params[:sender_id],params[:recipient_id]).present?
			@chat = Chat.between(params[:sender_id],params[:recipient_id]).first
		else
			@chat = Chat.create!(chat_params)
		end
		redirect_to @chat
	end

	private
	def chat_params
		params.permit(:sender_id, :recipient_id )
	end
end
