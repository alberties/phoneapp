class FriendshipsController < ApplicationController
	def create
		@friend = current_user.friendships.create(friend_params)
		respond_to do |format|
      		format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/create.js.erb" }
		end
	end

	def destroy
		@friend = Friendship.find(params[:id])
		@friend.destroy
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found } 
			format.js { render :file => "/contacts/destroy.js.erb" }
		end

	end

	def show
		@friend = Friendship.find(params[:id])
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }  
			format.json { head :no_content }
			format.js   { render :file => "/contacts/show.js.erb" }
		end
	end

	def edit
		@friend = Friendship.find(params[:id])
	end

	def update
		@friend = Friendship.find(params[:id])
		@friend.update(contact_params)

 		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js
		end
	end

	private
		def friend_params
			params.require(:friendship).permit(:user_id, :friend_id)
		end
end
