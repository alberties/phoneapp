class GroupsController < ApplicationController
	before_filter do 
		if !current_user
			redirect_to unauthenticated_root_path, alert: "Acceso denegado"
		elsif !current_user.admin?
			redirect_to authenticated_root_path, alert: "Acceso denegado"
		end
  	end

	def index
		@groups = Group.all
		current_user.update(:status_id => 2)
	end

	def destroy
		@group = Group.find(params[:id])
		@group.destroy

		if @group.destroy
	        redirect_to groups_path, notice: "Grupo eliminado con éxito"
	    end
	end

	def new
		@group = Group.new		
	end

	def create		
		@group = Group.new(group_params)

		if @group.save
			redirect_to(groups_path, notice: "Grupo creado con éxito")
		else
			flash.now[:error] = @group.errors.full_messages
			render :action => :new
		end
	end

	def edit
		@group = Group.find(params[:id])
	end

	def update
		@group = Group.find(params[:id])
 		@group.update(group_params)

 		if @group.save
	  		redirect_to :back, notice: "Grupo modificado correctamente"
		else
			flash.now[:error] = @group.errors.full_messages
			render :action => :edit
		end
		
	end

	def show
		@group = Group.find(params[:id])
		@users = @group.users.all
	end

	private
		def group_params
			params.require(:group).permit(:name, :description, :from_webservice, :outsider_ids => [], :user_ids => [])
		end
end
