class LinesController < ApplicationController
	def create
		@chat = Chat.find(params[:chat_id])
		@line = @chat.lines.build(line_params)
		@line.user_id = current_user.id
		@line.save!
	end

	private
		def line_params
			params.require(:line).permit(:body)
		end
end
