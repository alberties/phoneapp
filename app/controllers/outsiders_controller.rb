class OutsidersController < ApplicationController
	def create
		if params[:outsider][:first_name]
			@outsider = current_user.outsiders.create(outsider_params)
			if @outsider.save
				redirect_to(:back, notice: "Contacto agregado")
			else
				flash.now[:error] = @user.errors.full_messages
			end
		else
			@outsider = Outsider.find(params[:outsider][:outsider_id])
			current_user.outsiders << @outsider
			respond_to do |format|
				format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
				format.js { render :file => "/contacts/create.js.erb" }
			end
		end
	end

	def disjoint
		@outsider = Outsider.find(params[:id])
		@group = Group.find(params[:group_id])
		@outsider.groups.delete(@group)
		
		# Si el contacto externo no pertenece a ningún usuario ni a ningún grupo, nadie podrá acceder a él, 
		# por lo que se elimina.
		if @outsider.groups.empty? && @outsider.users.empty?
			@outsider.destroy
		end

		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/disjoint.js.erb" }
		end
	end

	def join
		@outsider = Outsider.find(params[:id])
		@group = Group.find(params[:group_id])
		@outsider.groups << @group

		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/join.js.erb" }
		end
	end

	def destroy
		@outsider = Outsider.find(params[:id])
		current_user.outsiders.delete(@outsider)

		# Si el contacto externo no pertenece a ningún usuario ni a ningún grupo, nadie podrá acceder a él, 
		# por lo que se elimina.
		if @outsider.groups.empty? && @outsider.users.empty?
			@outsider.destroy
		end

		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/destroy.js.erb" }
		end
	end

	def show
		@outsider = Outsider.find(params[:id])
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js   { render :file => "/contacts/show.js.erb" }
		end
	end

	def update
		@outsider = Outsider.find(params[:id])
		@outsider.update(outsider_params)
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js   { render :file => "/contacts/update.js.erb" }
		end
	end

	def outsider_params
		params.require(:outsider).permit(:first_name, :last_name, :sip_id, :sip_server, :email, :user_id, :group_id, :group_ids => [])
	end
end
