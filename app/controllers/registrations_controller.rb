class RegistrationsController < Devise::RegistrationsController
	# before_filter :configure_permitted_parameters


	def new
		flash[:info] = 'Registro cerrado al público'
		redirect_to controller: 'home', action: 'index'
	end

	def create
		flash[:info] = 'Registro cerrado al público'
		redirect_to controller: 'home', action: 'index'
	end

	def update
		@previous_status = current_user.status.id
		self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
	    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

	    @resource_updated = update_resource(resource, account_update_params)
	    yield resource if block_given?
	    if @resource_updated
			sign_in resource_name, resource, bypass: true
			flash.now[:success] = "Perfil actualizado"
	    else
	    	flash.now[:error] = flash[:error].to_a.concat resource.errors.full_messages
	    end

	    respond_to do |format|
	    	format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
	    	format.js
		end
	    
	end

	def simple_update
		@user = current_user
		@user.update(user_params)
		if @user.save
			@has_changed = true
			flash.now[:success] = 'Perfil actualizado'
		else
			flash.now[:error] = @user.errors.full_messages.first
		end

		if params[:user][:status_id]
			respond_to do |format|
				format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
		    	format.js { render :file => "/registrations/update-status.js.erb" }
			end
		else
			respond_to do |format|
				format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
		    	format.js { render :file => "/registrations/update-avatar.js.erb" }
			end
		end
	end

	def cancel
		flash[:info] = 'No puede cancelar su cuenta'
		redirect_to controller: 'home', action: 'index'
	end

	protected
	    def after_update_path_for(resource)
	    	authenticated_root_path
	    end

		# def configure_permitted_parameters
		# 	devise_parameter_sanitizer.for(:account_update).push(:avatar, :status_id)
		# end

		def user_params
			params.require(:user).permit(:avatar, :status_id)
		end
end
