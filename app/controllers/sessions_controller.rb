class  SessionsController < Devise::SessionsController
  skip_before_filter :configure_permitted_parameters, :only => [:create]
  skip_before_action :set_user, :only => [:create]

  def create
    remote_sign_up
    current_user.update(:status_id => 1)
  end

  def destroy
    current_user.update(:status_id => 2)
    @update_id = current_user.id 
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message! :notice, :signed_out if signed_out
    yield if block_given?
    respond_to :js
  end

  private
    # Por defecto de Devise, comprueba en la base de datos si existe el recurso y lo logea redirigiendole a root
    def database_auth
      self.resource = warden.authenticate!(auth_options)
      set_flash_message!(:notice, :signed_in) 
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource, location: after_sign_in_path_for(resource)
    end

    # Lo primero que hacemos es llamar al webservice de la UCA y obtener todos los datos del usuario al que corresponda el email (result/soap_hash)
    ## Si no existe ningún usuario con el email introducido en el webservice, significa que o son datos incorrectos o es un usuario local ->
      #---Login de devise---#
    ## Si existe algún usuario con ese email en la UCA y la contraseña es válida, comprobamos si existe también en nuestra base de datos
    ### Si existe en nuestra base de datos actualizamos sus datos sip usando los del webservice por si hubiese cambiado algo desde el último logeo
    ### Si no existe en nuestra base de datos creamos un usuario usando los datos que hemos recibido del webservice
    ## Ahora comprobamos si existe algun grupo cuyo nombre sea el mismo que el del campo :unidad que recibimos del webservice
    ### Si existe el grupo; comprobamos si el usuario que hemos creado o actualizado pertenece ya a el, si no pertenece lo añadimos
    ### Si no existe el grupo; lo creamos y añadimos al usuario que hemos creado o actualizado al mismo
     #---Login de devise---#
    def remote_sign_up 
      require 'savon'
      client = Savon.client(
        wsdl: 'https://cau.uca.es/cau/cau.wsdl', 
        basic_auth: ['sip', 'S1Ps1pS1p865']
      )

      response = client.call(:autentica_usuario_sip, xml: 
      "<env:Envelope xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:tns='http://cau.uca.es/CauWS/' xmlns:env='http://schemas.xmlsoap.org/soap/envelope/'>
        <env:Body>
          <env:AutenticaUsuarioSIP>
            <uidOEmail>#{params[:user][:email]}</uidOEmail>
            <password>#{params[:user][:password]}</password>
          </env:AutenticaUsuarioSIP>
        </env:Body>
      </env:Envelope>")

      result = response.to_hash[:multi_ref]
      if result.nil?
        database_auth   
      else
        if User.exists?(email: params[:user][:email])
          update_user(result)
        else
          create_user(result)
        end
        check_groups(result)
      end
    end

    def create_user(soap_hash)
      @user = User.new(:email => params[:user][:email], :password => params[:user][:password], 
        :first_name => soap_hash[:nombre], :last_name => soap_hash[:apellido1],
        :sip_id => soap_hash[:extension_sip], :sip_server => soap_hash[:servidor_sip], :sip_password => soap_hash[:secreto_sip],  
        :from_webservice => true )
      @user.avatar_from_url(soap_hash[:url_foto])
      @user.save
    end

    def update_user(soap_hash)
      @user = User.where(:email => params[:user][:email]).first
      @user.update_attributes( :first_name => soap_hash[:nombre], :last_name => soap_hash[:apellido1],
        :sip_id => soap_hash[:extension_sip], :sip_server => soap_hash[:servidor_sip], :sip_password => soap_hash[:secreto_sip],  
        :from_webservice => true )
      @user.save
    end

    def add_user_to_group(user_email, group_id)
      @user = User.find_by_email(user_email)
      @group = Group.find(group_id)
      if !@group.users.include?(@user)
        @group.users << @user
      end
      database_auth
    end

    def delete_user_last_group(user_email)
      @user = User.find_by_email(user_email)
      @group = @user.groups.where(:from_webservice => true).first
      if !@group.nil?
        @group.users.delete(@user)
      end
    end

    def create_group(name, description, user_email)
      @group = Group.new(:name => name, :description => description, :from_webservice => true)
      @group.save
      add_user_to_group(user_email, @group.id)
    end

    def check_groups(soap_hash)
      if Group.exists?(name: soap_hash[:codigo_unidad])
        @group = Group.where(name: soap_hash[:codigo_unidad]).first
        add_user_to_group(soap_hash[:email], @group.id)
      else
        delete_user_last_group(soap_hash[:email])
        create_group(soap_hash[:codigo_unidad], soap_hash[:unidad], soap_hash[:email])
      end
    end 
end