class StatusesController < ApplicationController
	before_filter :except => :publish do 
		if !current_user
			redirect_to unauthenticated_root_path, alert: "Acceso denegado"
		elsif !current_user.admin?
			redirect_to authenticated_root_path, alert: "Acceso denegado"
		end
  	end

	def index
		@statuses = Status.all
		current_user.update(:status_id => 2)
	end

	def destroy
		@status = Status.find(params[:id])
		@status.destroy

		if @status.destroy
	        redirect_to statuses_path, notice: "Estado eliminado con éxito"
	    end
	end

	def new
		@status = Status.new		
	end

	def create		
		@status = Status.new(status_params)

		if @status.save
			redirect_to(statuses_path, notice: "Estado creado con éxito")
		else
			flash.now[:error] = @status.errors.full_messages
			render :action => :new
		end
	end

	def edit
		@status = Status.find(params[:id])
	end

	def update
		@status = Status.find(params[:id])
 		@status.update(status_params)

 		if @status.save
	  		redirect_to :back, notice: "Estado modificado correctamente"
		else
			flash.now[:error] = @status.errors.full_messages
			render :action => :edit
		end
		
	end

	def show
		@status = Status.find(params[:id])
		@users = @status.users.all
	end

	def publish

		respond_to do |format|
			format.html
			format.js { render :file => "/layouts/publish_status.js.erb" }
		end
	end

	private
		def status_params
			params.require(:status).permit(:name, :color, :make_calls, :receive_calls, :use_chat, :by_default)
		end
end
