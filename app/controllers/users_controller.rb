class UsersController < ApplicationController
	# #Solo pueden acceder a las vistas de este controlador los administradores
	# before_filter except: [:show, :join, :disjoint] do 
	# 	if !current_user
	# 		redirect_to unauthenticated_root_path, alert: "Acceso denegado"
	# 	elsif !current_user.admin?
	# 		redirect_to authenticated_root_path, alert: "Acceso denegado"
	# 	end
 #  	end
  	
 	def disjoint
		@user = User.find(params[:id])
		@group = Group.find(params[:group_id])
		@user.groups.delete(@group)
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/disjoint.js.erb" }
		end
	end

	def join
		@user = User.find(params[:id])
		@group = Group.find(params[:group_id])
		@user.groups << @group
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.js { render :file => "/contacts/join.js.erb" }
		end
	end

	def index
		@users = User.all
		current_user.update(:status_id => 2)
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy
		redirect_to(:back, notice: "Usuario eliminado")
	end
	
	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
	  		redirect_to(users_path, notice: "Usuario creado con éxito")
		else
			flash.now[:error] = @user.errors.full_messages
			render :action => :new

		end
	end

	def show
		@user = User.find(params[:id])
		respond_to do |format|
			format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
			format.json { head :no_content }
			format.js   { render :layout => false }
		end
	end

	def edit
		@user = User.find(params[:id])
		session[:prev_url] = request.referer
	end

	def update
		@user = User.find(params[:id])
		params[:user].delete(:password) if params[:user][:password].blank?
		params[:user].delete(:sip_password) if params[:user][:sip_password].blank?
		@user.update(user_params)
		sign_in(@user, :bypass => true) if @user == current_user

 		if @user.save	
 			redirect_to(session[:prev_url], notice: "Actualizado con éxito")
		else
			flash.now[:error] = @user.errors.full_messages
			render :action => :edit
		end
	end

	private
		def user_params
			params.require(:user).permit(:first_name, :last_name, :phone, :mobile_phone, :email, :password, :admin, 
				:sip_id, :sip_server, :sip_name, :sip_password, :ws_server, :status_id, :from_webservice, :group_ids => [] )
		end
end