module ApplicationHelper
	def devise_flash
		if controller.devise_controller? && resource.errors.any?
			flash.now[:error] = flash[:error].to_a.concat resource.errors.full_messages
			flash.now[:error].uniq!
		end
	end

	def publish_status
		javascript_tag("$.ajax({ url: '/status/publish', type: 'get', dataType:'script' });")
	end
end
