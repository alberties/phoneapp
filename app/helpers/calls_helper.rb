module CallsHelper
	def interlocutor_name(call)
		if call.external == false
			return call.interlocutor.name
		else
			if current_user == call.caller
				return "Llamada externa"
			else
				return params[:caller_sip_name]
			end
		end
	end

	def interlocutor_avatar(call)
		if call.external == false
			return call.interlocutor.avatar
		else
			return 'icons/default-avatar.png'
		end
	end

	def interlocutor_phone(call)
		if call.external == false
			return call.interlocutor.sip_id
		else
			return params[:caller_sip_id]
		end
	end

	def interlocutor_email(call)
		if call.external == false
			return call.interlocutor.email
		else
			return "No disponible"
		end
	end

	def caller_sip(call)
		if call.caller
			return call.caller.sip_id
		else
			return params[:caller_sip_id]
		end
	end
end
