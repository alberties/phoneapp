module HomeHelper
	##-- call_to --##
	# target => usuario o contacto al que intentamos llamar
	# type => video o audio, tipo de llamada
	# color => tipo de botón bootstrap (primary, warning, success...)
	# user_type => dos valores: [friend, outsider]
	def call_to (target, type, color, external)
		icon = "fa-phone" 
		icon = "fa-video-camera" if type == "video"
		if current_user.status.make_calls == true
			link_to calls_path( 
					:type => type, 
					:number => target.sip_id, :server => target.sip_server,
					:external => external
				), 
				:remote => true, :method => :post,
				data: {disable_with: "00"},
				:class => 'btn btn-'+color+' btn-xs list__icon start-call '+'start-call_'+ target.sip_id.to_s do
					content_tag(:i, "", class: "fa " + icon + " fa-lg").html_safe
				end
		else
			content_tag(:a, :href=>"javascript:void(0)",:onclick=>"return false;", :class => "btn btn-xs list__icon a-disabled btn-"+color) do
				content_tag(:i, "", class: "fa " + icon + " fa-lg").html_safe
    		end
		end
	end

	def chat_with (target, icon, color)
		if current_user.status.use_chat == true
			link_to chats_path(
				:sender_id => current_user.id, :recipient_id => target.id),
				:remote => true, :method => :post, 
				:class => 'btn btn-'+color+' btn-xs list__icon' do
					content_tag(:i, "", class: "fa " + icon + " fa-lg").html_safe
				end
		else
			content_tag(:a, :href=>"javascript:void(0)",:onclick=>"return false;", :class => "btn btn-xs list__icon a-disabled btn-"+color) do
				content_tag(:i, "", class: "fa " + icon + " fa-lg").html_safe
			end
		end
	end

	def break_friendship (target, type)
		@path = outsider_path(target)
		@path = friendship_path(target) if type == "friend"

		link_to @path, 
			:remote => true, method: :delete, 
			:class => 'btn btn-danger btn-xs list__icon pull-right', 
			data: { :confirm => 'El contacto se eliminará. ¿Continuar?' } do
				content_tag(:i, "", class: "fa fa-trash fa-lg").html_safe
			end
	end

	def befriend (target, type)
		@path = outsider_path(:outsider => {:outsider_id => target.id}) if type == "outsider"
		@path = friendships_path(:friendship => { :friend_id => target.id }) if type == "friend"

		link_to @path, 
			:method => :post, remote: :true,
			:class => "btn btn-primary btn-xs list__icon pull-right add#{target.id}",
			:id => 'add'+target.id.to_s,
			'data-disable-with' => '<i class="fa fa-lg fa-plus-square"/>' do
				content_tag(:i, "", class: "fa fa-plus-square fa-lg").html_safe
			end
	end

	def remove_from_group (target, type)
		@path = disjoint_outsider_path(target, :group_id => group.id) if type == "outsider"
		@path = disjoint_user_path(target, :group_id => group.id) if type == "user" || type == "friend"

		link_to @path, 
			remote: :true, 
			:class => 'btn btn-danger btn-xs list__icon', 
			data: { :confirm => 'El contacto saldrá del grupo. ¿Continuar?' } do
				content_tag(:i, "", class: "fa fa-times-circle fa-lg").html_safe
			end
	end
end
