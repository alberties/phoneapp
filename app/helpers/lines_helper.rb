module LinesHelper
	def self_or_other(line)
		line.user == current_user ? "chat__line--self" : "chat__line--other"
	end
end
