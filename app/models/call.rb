class Call < ActiveRecord::Base
	attr_accessor :current_user_id

	belongs_to :caller, :class_name => "User", :foreign_key => :caller_id
	belongs_to :callee, :class_name => "User", :foreign_key => :callee_id
	belongs_to :callee_outsider, :class_name => "Outsider", :foreign_key => :callee_out_id

	def interlocutor
		if self.caller.id == current_user_id
			if self.callee
				return self.callee
			else
				return self.callee_outsider
			end
		else
			return self.caller
		end
	end

	def callee_sip_id
		string = callee_sip.split('@', 2)
		return string[0]
	end

	def get_duration
		return (Time.now - self.created_at.to_time)/1.seconds
	end
end
