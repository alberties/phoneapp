class Friendship < ActiveRecord::Base
	# auto_strip_attributes :first_name, :squish => true
	# auto_strip_attributes :last_name, :squish => true
	# auto_strip_attributes :email, :squish => true
	# auto_strip_attributes :phone, :squish => true
	# auto_strip_attributes :mobile_phone, :squish => true

	belongs_to :user
	belongs_to :friend, :class_name => "User"

	def name
		"#{first_name} #{last_name}"
	end

	def match_attributes
		return attributes.except('user_id', 'id', 'created_at', 'updated_at', 'first_name', 'last_name')
	end

	# def friend
	# 	return User.where(:id => self.friend_id)
	# end
end
