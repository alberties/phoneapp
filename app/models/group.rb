class Group < ActiveRecord::Base
	has_many :user_memberships, :dependent => :destroy
	has_many :users, :through => :user_memberships

	has_many :outsider_memberships, :dependent => :destroy
	has_many :outsiders, :through => :outsider_memberships

	validates :name, :presence => true
	validates_length_of :name, :maximum => 30
	validates_length_of :description, :maximum => 70
	validates :description, :presence => true
end
