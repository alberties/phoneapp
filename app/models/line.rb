class Line < ActiveRecord::Base
	belongs_to :chat
	belongs_to :user

	after_save do |record|
		if Line.where(:user => self.user).count >= 50
			Line.where(:user => self.user).order('created_at asc').first.destroy
		end
	end
end
