class Outsider < ActiveRecord::Base
	has_many :user_outsiders, :dependent => :destroy
	has_many :users, :through => :user_outsiders, :dependent => :destroy

	has_many :outsider_memberships, :dependent => :destroy
	has_many :groups, :through => :outsider_memberships

	has_many :sessions, :dependent => :destroy
	has_many :calls, :through => :sessions, :dependent => :destroy

	def name
		"#{first_name} #{last_name}"
	end
end
