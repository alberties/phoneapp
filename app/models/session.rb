class Session < ActiveRecord::Base
	belongs_to :user
	belongs_to :outsider
	belongs_to :call, :dependent => :delete
end
