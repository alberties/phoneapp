require "open-uri"

class User < ActiveRecord::Base
	attr_accessor :current_user_id

	has_attached_file :avatar, validate_media_type: false
	validates_attachment_size :avatar, :less_than => 1.megabytes
	validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'document/do']

	validates :first_name, :presence => true
	validates :last_name, :presence => true	

	has_many :user_memberships, :dependent => :delete_all
	has_many :groups, :through => :user_memberships
	has_many :admin_memberships, -> { group_admin }, :class_name => 'UserMembership'
	has_many :admin_groups, :source => :group, :through => :admin_memberships

	has_many :friendships, :dependent => :delete_all
	has_many :friends, :through => :friendships, :dependent => :delete_all
	has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id", :dependent => :delete_all
	has_many :inverse_friends, :through => :inverse_friendships, :source => :user, :dependent => :delete_all

	has_many :user_outsiders, :dependent => :delete_all
	has_many :outsiders, :through => :user_outsiders, :dependent => :delete_all

	has_many :caller_calls, :class_name => "Call", :dependent => :delete_all, :foreign_key => :caller_id
	has_many :callee_calls, :class_name => "Call", :dependent => :delete_all, :foreign_key => :callee_id

	has_many :chats_started, :class_name => "Chat", :dependent => :delete_all, :foreign_key => :sender_id
	has_many :chats_received, :class_name => "Chat", :dependent => :delete_all, :foreign_key => :recipient_id

	belongs_to :status

	devise :database_authenticatable, :registerable, 
    	   :recoverable, :rememberable, :trackable, :validatable

   	def avatar_from_url(url)
	    self.avatar = open(url)
	end

    def chats
		Chat.where("sender_id = ? OR recipient_id = ?", self.id, self.id)
	end

	def name
		"#{first_name} #{last_name}"
	end

	def unanswered_calls
		self.callee_calls.where(answered: [false, nil])
	end

	def sip_uri
		"#{sip_id}"+"@"+"#{sip_server}"
	end
end