class UserMembership < ActiveRecord::Base
	belongs_to :user
	belongs_to :group

	scope :group_admin, -> {
		where(:admin => true)
	}
end
