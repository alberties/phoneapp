Rails.application.routes.draw do
  devise_for :users, :path_prefix => 'phone', :controllers => { :registrations => "registrations", sessions: "sessions"}
  get '/user/join/:id' => 'users#join', :as => :join_user
  get '/user/disjoint/:id' => 'users#disjoint', :as => :disjoint_user
  resources :users

  resources :groups

  get '/status/publish' => 'statuses#publish', :as => :publish_status
  resources :statuses

  resources :friendships

  get '/outsider/join/:id' => 'outsiders#join', :as => :join_outsider
  get '/outsider/disjoint/:id' => 'outsiders#disjoint', :as => :disjoint_outsider
  resources :outsiders

  get '/calls/active/:id' => 'calls#caller_side', :as => :call_caller_active
  get '/calls/getid' => 'calls#get_call_id', :as => :call_get_id
  get '/call/create', :to => "calls#create", :as => :create_call
  resources :calls

  resources :chats
  
  resources :lines

  devise_scope :user do
    put   "/phone/users" => "devise/registrations#update", :as => :update_user_registration
    
    patch   "/phone/users/supdate" => "registrations#simple_update", :as => :update_user_simply
    patch   "/phone/users/status" => "registrations#update_status", :as => :update_user_status

    authenticated :user do
      root 'home#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

end

