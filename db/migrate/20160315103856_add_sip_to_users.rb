class AddSipToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :sip_id, :string
  	add_column :users, :sip_server, :string
  	add_column :users, :sip_password, :string
  	add_column :users, :status_id, :integer, :default => 2
    add_column :users, :ws_server, :string
  end
end
