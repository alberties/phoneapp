class CreateUserMemberships < ActiveRecord::Migration
  def change
    create_table :user_memberships do |t|
      t.integer :user_id
      t.integer :group_id
      t.boolean :admin, :default => false

      t.timestamps null: false
    end
  end
end
