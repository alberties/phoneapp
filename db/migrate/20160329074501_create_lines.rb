class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.text :body
      t.integer :user_id
      t.integer :chat_id

      t.timestamps null: false
    end
  end
end
