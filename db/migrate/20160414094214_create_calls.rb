class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
    	t.integer :caller_id
    	t.integer :callee_id
      t.integer :callee_out_id
    	t.string  :callee_sip
    	t.boolean :answered  , :default => false
		t.string  :call_type
		t.boolean :external  , :default => false
		t.integer :duration  , :default => 0

		t.timestamps null: false
    end
  end
end
