class CreateOutsiders < ActiveRecord::Migration
  def change
    create_table :outsiders do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :sip_id
      t.string :sip_server
      t.string :ws_server
      t.string :avatar, :default => 'icons/default-avatar.png'

      t.timestamps null: false
    end
  end
end
