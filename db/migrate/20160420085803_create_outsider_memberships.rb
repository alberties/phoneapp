class CreateOutsiderMemberships < ActiveRecord::Migration
  def change
    create_table :outsider_memberships do |t|
      t.integer :outsider_id
      t.integer :group_id

      t.timestamps null: false
    end
  end
end
