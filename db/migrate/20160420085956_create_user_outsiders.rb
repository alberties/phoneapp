class CreateUserOutsiders < ActiveRecord::Migration
  def change
    create_table :user_outsiders do |t|
      t.integer :user_id
      t.integer :outsider_id

      t.timestamps null: false
    end
  end
end
