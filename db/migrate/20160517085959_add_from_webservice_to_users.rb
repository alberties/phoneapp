class AddFromWebserviceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :from_webservice, :boolean, default: false
  end
end
