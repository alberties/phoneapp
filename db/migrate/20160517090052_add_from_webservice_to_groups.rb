class AddFromWebserviceToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :from_webservice, :boolean, default: false
  end
end
