class CreateStatuses < ActiveRecord::Migration
	def change
		create_table :statuses do |t|
			t.string :name, null: false
			t.string :color, null: false
			t.boolean :by_default, :default => false
			t.boolean :receive_calls, :default => false
			t.boolean :make_calls, :default => false
			t.boolean :use_chat, :default => false

			t.timestamps null: false
		end
	end
end
